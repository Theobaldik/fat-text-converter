import setuptools

with open("README.md", "r") as fil:
    long_description = fil.read()
with open("requirements.txt", "r") as fil:
    requirements = [line.strip() for line in fil]

setuptools.setup(
    name="ft_converter",
    version="0.0.4",
    author="Filip Klopec",
    author_email="filipklopec@gmail.com",
    description="Fat Text Converter an application for converting text files with various encodings.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU GPLv3",
        "Operating System :: OS Independent",
    ],
    python_requires=">=3.6",
    install_requires=requirements,
)
