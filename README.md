# Fat Text Converter

Fat Text Converter (FTC) is a basic application written in python. It allows you convert text files between different codecs like UTF-8, UTF-16, ASCII and so on.

For more information read the [documentation](https://theobaldik.gitlab.io/fat-text-converter/).

## Installing FTC

The easiest way to install FTC to your computer is just download the archive folder withing the [bin](/bin) matching the OS you use. Then you extract the folder and run ftc.exe (Windows).
You can also use these download links:

- **Windows (zip archive)**: **[ftc_0_0_4.zip](https://gitlab.com/Theobaldik/fat-text-converter/-/raw/master/bin/win/ftc_0_0_4.zip)**
- **Linux**: not available yet
- **Mac OS**: not available yet

If you have [python 3.6](https://www.python.org/) and above with [pip](https://pypi.org/) installed on your computer, you can of course use the source code to run the program. First install all requied packages listed in [requirements.txt](/requirements.txt) file using `pip install -r requirements.txt` command. Then just run `python .` within the *ft_converter* directory.

You can also use pip to install FTC to your comuputer using [setup.py](/setup.py) file. Note that for this option you need to download the whole repository except the *bin* folder. Then within the repository folder run `pip install .` command. This will automatically install all requied packages and the *ft-converter* package into your *site-packages* folder.

## Usage of FTC

Make sure you have added directory with FTC executable if you want to use its CLI.

For more detailed description of usage and confguration see the [documentation](https://theobaldik.gitlab.io/fat-text-converter/usage).

Example of encoding multiple files using *ascii* encoding and *override* mode (input files will be overriden).

```bash
ftc encode -o override -e ascii example0.txt example1.txt example2.txt
```

Example of encoding multiple files using default encoding and default mode.

```bash
ftc example0.txt example1.txt
```
