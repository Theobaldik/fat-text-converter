# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [Unreleased]

### Added

- Batch converter for directories

## [0.0.4] - 2021-01-08

### Added

- Input encoding and minimal confidence in configuration
- Set default input encoding as codepage for system language

### Changed

- Default directory for error.log and config.json set to APPDATA/Fat Camel Studio/Fat Text Converter

## Fixed

- Issues with input file encoding recognition
- 