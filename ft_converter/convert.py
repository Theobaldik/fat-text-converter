import os
import logger
import constants
import chardet

def get_outfile(in_file, outfile_mode):
    """
    Gets output file path.
    :param in_file: input file path
    :param outfile_mode: override | same-as-source | default-dir
    :returns: output file path
    """
    if outfile_mode == 'override':
        return in_file
    elif outfile_mode == 'same-as-source':
        path, ext = os.path.splitext(in_file)
        return f'{path} (encoded){ext}'
    else:
        return os.path.join(constants.DEFAULT_OUTDIR, os.path.basename(in_file))

def get_encoding(filepath):
    """
    Gets encoding of given file.
    :param filepath: path of file to be analyzed
    :returns: encoding string
    """
    with open(filepath, 'rb') as fil:
        result = chardet.detect(fil.read())
        print(result)
        if result['confidence'] <= constants.DEFAULT_MIN_CONFIDENCE:
            return None
        return result['encoding']

def normalize_encoding(enc: str):
    """
    Gets normalized encoding name.
    :param enc: original encoding
    :returns: normalized encoding
    """
    return enc.replace('Windows-', 'cp')

def convert_file(in_file, outfile_mode, in_encoding, out_encoding):
    """
    Converts given file using given mode and encoding.
    :param in_file: input file path
    :param outfile_mode: override | same-as-source | default-dir
    :param out_encoding: output file encoding
    :returns: True if succeed, False otherwise
    """
    out_file = get_outfile(in_file, outfile_mode)   
    try:        
        buffer = ''
        with open(in_file, 'r', encoding=in_encoding) as fil:
            buffer = fil.read()
        with open(out_file, 'w+', encoding=out_encoding) as fil:
            fil.truncate(0)            
            try:                
                fil.write(buffer)
            except:
                logger.log_error()
        return True
       
    except Exception:
        logger.log_error()
        return False

if __name__ == '__main__':
    pass
