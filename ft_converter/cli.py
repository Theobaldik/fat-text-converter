import click
import constants
import convert
import logger
import os

@click.group(invoke_without_command=True, no_args_is_help=True)
@click.option('-v', '--version', is_flag=True, help='Show FTC version and exit.')
@click.option('--show-log', is_flag=True, help='Shows log file content and exit.')
@click.option('--clear-log', is_flag=True, help='Clear log file content and exit.')
@click.option('--clist', is_flag=True, help='Show all available encodings and exit.')
def cli(version, show_log, clear_log, clist):
    """
    Fat Text Converter converts text files with various endodings types.\n
    For quick encoding open files with this app.
    """
    if version:
        click.echo(f'Fat Text Converter version {constants.VERSION}')
        return    
    if show_log:
        content = logger.get_log()
        if content:
            click.echo(content)
        else:
            click.echo("Log file is empty.")
        return
    if clear_log:
        logger.clear_log()
        return    
    if clist:
        is_first = True
        for codec in constants.encodings:
            if is_first:
                print(codec, end='')
                is_first = False 
            else:
                print('  ', codec, end='')
    
@cli.command(no_args_is_help=True)
@click.option('-o', '--out', type=click.Choice(constants.outfile_modes), default=constants.DEFAULT_OUTFILE_MODE, help='how to save output file')
@click.option('-e', '--enc', default=constants.DEFAULT_ENCODING, help='output encoding, run `ftc --clist` to show available encodings')
@click.argument('filepaths', nargs=-1, type=click.Path(exists=True, readable=True, file_okay=True), required=True)
def encode(out, enc, filepaths):
    """Encodes given files using given options."""
    if enc not in constants.encodings:
        click.echo(f'{enc} is not valid encoding. Run `ftc --clist` to show all valid encodings.')
        return
    i = 0
    count = len(filepaths)
    errors = 0
    for filepath in filepaths:  
        i += 1
        if out == 'default-dir' and not constants.DEFAULT_OUTDIR:
            constants.DEFAULT_OUTDIR = click.prompt('Default output directory was not set yet. Enter default directory', type=click.Path())
            constants.create_default_config()

        in_enc = convert.get_encoding(filepath)
        if in_enc == None:
            if constants.DEFAULT_IN_ENCODING != '-':
                in_enc = constants.DEFAULT_IN_ENCODING
            else:
                in_enc = click.prompt(f'The encoding of {filepath} cannot be recognized. Enter encoding manually')
                while in_enc not in constants.encodings:
                    in_enc = click.prompt(f'{in_enc} is not valid encoding. Enter another encoding')

        click.echo(f'Encoding {i} of {count}: {os.path.basename(filepath)}')        
        if not convert.convert_file(filepath, out, in_enc, enc):        
            click.echo(f'Error during {os.path.basename(filepath)} encoding. Check error.log file for more information.')
            errors += 1        
        
    click.echo(f'{i-errors} of {count} files encoded successfully! {errors} errors occured.')        

@cli.command(no_args_is_help=True)
@click.option('-o', '--out', type=click.Choice(constants.outfile_modes), help='how to save output file')
@click.option('-e', '--enc',  help='output encoding, run `ftc --clist` to show available encodings')
@click.option('-i', '--inenc', help='default input encoding, set `-` for none encoding, used for unrecognized encodings')
@click.option('-d', '--outdir', type=click.Path(), help='default output directory')
@click.option('-c', '--confidence', type=float, help='minimal confidence for input encoding recognition, between 0.0 and 1.0')
@click.option('--reset', is_flag=True, help='Reset to default setting and exit.')
def config(out, enc, inenc, outdir, confidence, reset):
    """Configure FTC settings."""
    if reset:
        constants.reset_config()
        return
    if enc:
        if enc not in constants.encodings:
            click.echo(f'{enc} is not valid encoding. Run `ftc --clist` to show all valid encodings.')
            return
        constants.DEFAULT_ENCODING = enc
    if inenc:        
        if inenc not in constants.encodings and inenc != '-':
            click.echo(f'{inenc} is not valid encoding. Run `ftc --clist` to show all valid encodings.')
            return
        constants.DEFAULT_IN_ENCODING = inenc
    if confidence:
        if confidence < 0.0 or confidence > 1.0:
            click.echo('Confidence value must be between 0.0 and 1.0.')
            return
        constants.DEFAULT_MIN_CONFIDENCE = confidence
    if out:
        constants.DEFAULT_OUTFILE_MODE = out        
    if outdir:
        constants.DEFAULT_OUTDIR = outdir
    if out or enc or outdir or inenc or confidence:
        constants.create_default_config()    

if __name__ == '__main__':
    pass
