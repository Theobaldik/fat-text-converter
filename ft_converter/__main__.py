import cli, sys, os

def main():
    """ Entry point of the app."""
    quick_convert = False
    # Encode using default setting if only paths as arguments are given.
    if len(sys.argv) > 1:        
        if sys.argv[1] not in cli.cli.commands.keys():
            if os.path.exists(sys.argv[1]):
                cli.encode()
                quick_convert = True            
    if not quick_convert:
        cli.cli()

if __name__ == "__main__":
    main()
