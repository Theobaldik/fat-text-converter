import json
import logger
import os
import locale

VERSION = '0.0.4'

FTC_ROOT = os.path.join(os.getenv('APPDATA'), 'Fat Camel Studio', 'Fat Text Converter')
try:
    os.makedirs(FTC_ROOT)
except FileExistsError:
    pass

LOG_PATH = os.path.join(FTC_ROOT, 'error.log')
CONFIG_PATH = os.path.join(FTC_ROOT, 'config.json')

outfile_modes = ['override', 'same-as-source', 'default-dir']

encodings = ['ascii', 'big5', 'big5hkscs', 'cp037', 'cp273', 'cp424', 'cp437', 'cp500', 'cp720',
        'cp775', 'cp850', 'cp852', 'cp855', 'cp856', 'cp857', 'cp858', 'cp860',
        'cp861', 'cp862', 'cp863', 'cp864', 'cp865', 'cp866', 'cp869', 'cp874', 'cp875', 'cp932',
        'cp949', 'cp950', 'cp1006', 'cp1026', 'cp1125', 'cp1140', 'cp1250', 'cp1251', 'cp1252',
        'cp1253', 'cp1254', 'cp1255', 'cp1256', 'cp1257', 'cp1258', 'euc_jp', 'euc_jis_2004',
        'euc_jisx0213', 'euc_kr', 'gb2312', 'gbk', 'gb18030', 'hz', 'iso2022_jp', 'iso2022_jp_1',
        'iso2022_jp_2', 'iso2022_jp_2004', 'iso2022_jp_3', 'iso2022_jp_ext', 'iso2022_kr', 'latin_1',
        'iso8859_2', 'iso8859_3', 'iso8859_4', 'iso8859_5', 'iso8859_6', 'iso8859_7', 'iso8859_8',
        'iso8859_9', 'iso8859_10', 'iso8859_11', 'iso8859_13', 'iso8859_14', 'iso8859_15', 
        'iso8859_16', 'johab', 'koi8_r', 'koi8_t', 'koi8_u', 'kz1048', 'mac_cyrillic', 'mac_greek',
        'mac_iceland', 'mac_latin2', 'mac_roman', 'mac_turkish', 'ptcp154', 'shift_jis', 'shift_jis_2004',
        'shift_jisx0213', 'utf_32', 'utf_32_be', 'utf_32_le', 'utf_16', 'utf_16_be', 'utf_16_le', 'utf_7',
        'utf_8', 'utf_8_sig', 'idna', 'mbcs', 'oem', 'palmos', 'punycode', 'rot_13']

DEFAULT_MIN_CONFIDENCE = 0.8
DEFAULT_IN_ENCODING = 'cp' + locale.getlocale()[1]
DEFAULT_ENCODING = 'utf_8'
DEFAULT_OUTFILE_MODE = 'same-as-source'
DEFAULT_OUTDIR = ''

def create_default_config():
    """Creates configuration file using default setting."""
    config_dict = {
        'default-encoding': DEFAULT_ENCODING,
        'default-in-encoding': DEFAULT_IN_ENCODING,
        'default-min-confidence': DEFAULT_MIN_CONFIDENCE,
        'default-outfile-mode': DEFAULT_OUTFILE_MODE,
        'default-outdir': ''
    }
    try:
        with open(CONFIG_PATH, 'w') as fil:
            json.dump(config_dict, fil)
    except:
        logger.log_error()

def load_constants():
    """Loads settings from configuration file."""
    global DEFAULT_ENCODING, DEFAULT_OUTFILE_MODE, DEFAULT_OUTDIR, DEFAULT_MIN_CONFIDENCE, DEFAULT_IN_ENCODING
    try:
        with open(CONFIG_PATH, 'r') as fil:
            json_fil = json.load(fil)
            DEFAULT_MIN_CONFIDENCE = json_fil['default-min-confidence']
            DEFAULT_ENCODING = json_fil['default-encoding']
            DEFAULT_IN_ENCODING = json_fil['default-in-encoding']
            DEFAULT_OUTFILE_MODE = json_fil['default-outfile-mode']
            DEFAULT_OUTDIR = json_fil['default-outdir']
    except FileNotFoundError:
        create_default_config()
    except:
        logger.log_error()

def reset_config():
    """Recover default settings."""
    try:
        os.remove(CONFIG_PATH)
        load_constants()
    except:
        load_constants()

load_constants()

if __name__ == '__main__':
    pass
